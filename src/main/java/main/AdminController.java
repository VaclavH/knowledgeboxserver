/**
 *  @author Vaclav Hnizda
 *	Knowledge Box
 *	Project 2013-14
 *	SE491-591 - Software Engineering Studio
 */

package main;

/* Java Library Imports */
import java.util.Date;
import java.util.List;

import hibernate.APIKey;
import hibernate.Answer;
import hibernate.Column;
import hibernate.Group;
import hibernate.Question;
import hibernate.Table;
/* Knowledgebox Imports */
import hibernate.User;
import hibernate.UserRoleGroup;
import hibernateLogic.DatabasePro;
import hibernateLogic.SysAdminCheck;
import user.UserFactory;
import user.UserNameJson;




import org.springframework.validation.BindingResult;
/* Spring Imports */
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import Admin.APIKeyFactory;


/**
 * The UserController class is used to perform functions on users or create new users
 *
 */
@Controller
public class AdminController {
	
	/**
	 * This method is used to List all Users in the database
	 * @param adminKey is our master password
	 * @return is the User list
	 * @throws Exception if the table is empty
	 */
    @RequestMapping(value = "admin/listUsers", method=RequestMethod.GET)
	public @ResponseBody List<User> listUsers(
			@RequestParam(value="adminKey", 	 required=true)  String adminKey,
			@RequestParam(value="Orginization",  required=false) Integer orgId,	//Must be "Integer" = wrapper class not "int"
			@RequestParam(value="adminUserName", required=true)  String adminUserName) throws Exception {
		
    	if(SysAdminCheck.validAccount(adminUserName, adminKey)){
    		//Create local tool set
    		DatabasePro<User,Integer,String,Integer> myTools = new DatabasePro<User,Integer,String,Integer>();

	    	//Create new list	
    		List<User> allUsers;
    		
    		//Check to see if a group id was entered
//    		if(orgId != null)
//    		{
//    			//MUST use groupId --> userRoleGroup --> if match with user iDs --> return list of users 
//    			//Pull list
//    			allUsers = myTools.getTypeList(Table.User,Column.groupId,orgId);
//    		}
//    		else
//    		{
    			//Pull list
    			allUsers = myTools.getTypeList(Table.User);
//    		}
    		
    		// Pass on results
    		return allUsers;
    	}
    	else{ throw new Exception("Incorrect Admin Credentials!"); }
    }
    
    /**
	 * This method is used to List all Groups in the database
	 * @param adminKey is our master password
	 * @return is the User list
	 * @throws Exception if the table is empty
	 */
    @RequestMapping(value = "admin/listGroups", method=RequestMethod.GET)
	public @ResponseBody List<Group> listGroups(
			@RequestParam(value="adminKey", 	 required=true) String adminKey,
			@RequestParam(value="adminUserName", required=true) String adminUserName) throws Exception {
		
    	if(SysAdminCheck.validAccount(adminUserName, adminKey)){
    		//Create local tool set
    		DatabasePro<Group,String,Integer,Integer>  myTools = new DatabasePro<Group,String,Integer,Integer>();
    		//Pull list
    		List<Group> allGroups = myTools.getTypeList(Table.Group);
    		
    		// Pass on results
    		return allGroups;
    	}
    	else{ throw new Exception("Incorrect Admin Credentials!"); }
    }
	
    /**
     * This API will set up the key we have given Sabrina for her website
     * @param adminKey
     * @param adminUserName
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "admin/setSabrinaAPI", method=RequestMethod.GET)
    public @ResponseBody APIKey setSabrinaAPI(
    		@RequestParam(value="adminKey", 	 required=true) String adminKey,
    		@RequestParam(value="adminUserName", required=true) String adminUserName) throws Exception {
    	
    	if(SysAdminCheck.validAccount(adminUserName, adminKey)){
    		//Create local tool set
    		DatabasePro<APIKey,Integer,Integer,String>  myTools = new DatabasePro<APIKey,Integer,Integer,String>();
    		APIKey sabrinasKey;
    		
    		//Pull list
    		List<APIKey> allAPIKeys = myTools.getTypeList(Table.APIKey);
    		if(allAPIKeys.size() == 0)
    		{
    			// Make a new Key
    			sabrinasKey = APIKeyFactory.createDefinedKey(0, 7865, "DePaul Student");
    			// Now add it to the database
    			myTools.dataEntry(sabrinasKey);
    		}
    		else{ throw new Exception("Sabrina's API key is already ALL set up!!"); }
    		
    		//Pull list to confirm it's there
    		allAPIKeys = myTools.getTypeList(Table.APIKey,Column.userId,0);
    		
    		// Pass on results
    		return allAPIKeys.get(0);
    	}
    	else{ throw new Exception("Incorrect Admin Credentials!"); }
    }
    
	/**
	 * This method is used to return a list of all user names
	 * @param adminKey is our master password
	 * @return is the polished user list of names only
	 * @throws Exception if the table is empty
	 */
    @RequestMapping(value = "admin/listAllUsersNames", method=RequestMethod.GET)
	public @ResponseBody List<UserNameJson> listAllUsernames(
			@RequestParam(value="adminKey", 	 required=true) String adminKey,
			@RequestParam(value="adminUserName", required=true) String adminUserName) throws Exception {
		
    	if(SysAdminCheck.validAccount(adminUserName, adminKey)){
    		//Create local tool set
    		DatabasePro<User,String,Integer,Date> myTools = new DatabasePro<>();
    		//Pull list
    		List<User> allUsers = myTools.getTypeList(Table.User);
    		
    		//Use factory to create proper list
    		List<UserNameJson> polishedList = UserFactory.createUserNameList();
    		for ( User user : (List<User>) allUsers ) {
    			polishedList.add(new UserNameJson(user));
    		}
    		
    		// Pass on results
    		return polishedList;
    	}
    	else{ throw new Exception("Incorrect Admin Credentials!"); }
    }
    
    /**
     * This method is used to return a list of all User Role Group entries
     * @param adminKey is our master password
     * @return is the polished user list of names only
     * @throws Exception if the table is empty
     */
    @RequestMapping(value = "admin/listAllUserRoleGroups", method=RequestMethod.GET)
    public @ResponseBody List<UserRoleGroup> listAllUserRoleGroups(
    		@RequestParam(value="adminKey", 	 required=true) String adminKey,
    		@RequestParam(value="adminUserName", required=true) String adminUserName) throws Exception {
    	
    	if(SysAdminCheck.validAccount(adminUserName, adminKey)){
    		//Create local tool set
    		DatabasePro<UserRoleGroup, String, Integer, String> myTools = new DatabasePro<>();
    		//Pull list
    		List<UserRoleGroup> roleGroups = myTools.getTypeList(Table.UserRoleGroup);
    				
    		// Pass on results
    		return roleGroups;
    	}
    	else{ throw new Exception("Incorrect Admin Credentials!"); }
    }
    
    
    /**
     * This method is used to return a list of all Questions
     * @param adminKey is our master password
     * @return is the polished user list of names only
     * @throws Exception if the table is empty
     */
    @RequestMapping(value = "admin/listAllQuestions", method=RequestMethod.GET)
    public @ResponseBody List<Question> listAllQuestions(
    		@RequestParam(value="adminKey", 	 required=true) String adminKey,
    		@RequestParam(value="adminUserName", required=true) String adminUserName) throws Exception {
    	
    	if(SysAdminCheck.validAccount(adminUserName, adminKey)){
    		//Create local tool set
    		DatabasePro<Question, Integer, Integer, Integer> myTools = new DatabasePro<>();
    		//Pull list
    		List<Question> questions = myTools.getTypeList(Table.Question);
    		
    		// Pass on results
    		return questions;
    	}
    	else{ throw new Exception("Incorrect Admin Credentials!"); }
    }
    
    
    /**
     * This method is used to return a list of all Answers
     * @param adminKey is our master password
     * @return is the polished user list of names only
     * @throws Exception if the table is empty
     */
    @RequestMapping(value = "admin/listAllAnswers", method=RequestMethod.GET)
    public @ResponseBody List<Answer> listAllAnswers(
    		@RequestParam(value="adminKey", 	 required=true) String adminKey,
    		@RequestParam(value="adminUserName", required=true) String adminUserName) throws Exception {
    	
    	if(SysAdminCheck.validAccount(adminUserName, adminKey)){
    		//Create local tool set
    		DatabasePro<Answer, String, Date, Integer> myTools = new DatabasePro<>();
    		//Pull list
    		List<Answer> answer = myTools.getTypeList(Table.Answer);
    		
    		// Pass on results
    		return answer;
    	}
    	else{ throw new Exception("Incorrect Admin Credentials!"); }
    }
    
}
