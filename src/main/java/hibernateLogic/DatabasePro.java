package hibernateLogic;

import hibernate.Column;
import hibernate.Table;
import hibernate.User;

import java.util.List;

import org.hibernate.Session;

/**
 * This method is used to retrieve objects of from any table in hibernate
 * @author Vaclav Hnizda
 *
 * @param <Type> This is the Return type you want to get
 * @param <Column1> This is 1 of 3 items that you can specify by
 * @param <Column2> This is 2 of 3 items that you can specify by
 * @param <Column3> This is 3 of 3 items that you can specify by
 */
public class DatabasePro<HibernateTable,Column1,Column2,Column3> {
	
//-----------TABLE--ONLY-------------------------------------------------//	
	
	/**
	 * Use this search to return all entries in any table
	 * @return This returns a new list of your object(s). If none found it will be empty
	 */
	public List<HibernateTable> getTypeList(Table table)
	{
		QueryBuilderPro<Column1,Column2,Column3> myStringBuilder = new QueryBuilderPro<Column1,Column2,Column3>();
		
		
		// Pre-create string used for query search
		String hql = myStringBuilder.string(table);
		
		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Run a query to find the requested user
		@SuppressWarnings("unchecked")
		List<HibernateTable> result = (List<HibernateTable>)session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		return result;
	}	
	
	

//-----------ONE--COLUMN--ONLY-------------------------------------------------//	
	
	/**
	 * Use this search to return all entries based on 1 column criteria
	 * @return This returns a new list of your object(s). If none found it will be empty
	 */
	public List<HibernateTable> getTypeList(Table table, Column column1Name, Column1 search1)
	{
		QueryBuilderPro<Column1,Column2,Column3> myStringBuilder = new QueryBuilderPro<Column1,Column2,Column3>();
		
		
		// Pre-create string used for query search
		String hql = myStringBuilder.string(table,column1Name,search1);
		
		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Run a query to find the requested user
		@SuppressWarnings("unchecked")
		List<HibernateTable> result = (List<HibernateTable>)session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
			return result;
	}
	
	
	
//-----------TWO--COLUMNS--ONLY-------------------------------------------------//	
	
	/**
	 * Use this search to return all entries based on 2 column criteria
	 * @return This returns a new list of your object(s). If none found it will be empty
	 */
	public List<HibernateTable> getTypeList(Table table, Column column1Name, Column1 search1, 
												   Column column2Name, Column2 search2) 
	{
		QueryBuilderPro<Column1,Column2,Column3> myStringBuilder = new QueryBuilderPro<Column1,Column2,Column3>();
		
		// Pre-create string used for query search
		String hql = myStringBuilder.string(table,column1Name,search1,column2Name,search2);
		
		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Run a query to find the requested user
		@SuppressWarnings("unchecked")
		List<HibernateTable> result = (List<HibernateTable>)session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		return result;
	}
	
	
	
//-----------THREE--COLUMNS--ONLY-------------------------------------------------//	
	
	/**
	 * Use this search to return all entries based on 3 column criteria
	 * @return This returns a new list of your object(s). If none found it will be empty
	 */
	public List<HibernateTable> getTypeList(Table table, Column column1Name, Column1 search1, 
												   Column column2Name, Column2 search2,
												   Column column3Name, Column3 search3) 
			{
		QueryBuilderPro<Column1,Column2,Column3> myStringBuilder = new QueryBuilderPro<Column1,Column2,Column3>();
		
		// Pre-create string used for query search
		String hql = myStringBuilder.string(table, column1Name, search1,
												   column2Name, search2,
												   column3Name, search3);
		
		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Run a query to find the requested user
		@SuppressWarnings("unchecked")
		List<HibernateTable> result = (List<HibernateTable>)session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		return result;
			}
	
	
	/**
	 * Add an Object into the Database
	 * @param newType this is the object you are entering
	 */
	public HibernateTable dataEntry(HibernateTable newType){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Add the user
		session.save(newType);
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
	
		return newType;
	}
	
	/**
	 * Delete an Object entry from the database
	 * @param deleteType This is the object you are deleting
	 */
	public void dataRemoval(HibernateTable deleteType){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
    	session.beginTransaction();
    	
    	// Set up the deletion of the User
    	session.delete(deleteType);
    	
    	// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();

	}
	
	/**
	 * Update an Object entry to the database
	 * @param udateType this is the object you are updating
	 */
	public void dataUpdate(HibernateTable udateType){
		
		// Open a new session
		Session session = MyHibernateFactory.newSession();
    	session.beginTransaction();
    	
    	// Set up the update of the User
    	session.update(udateType);
    	
    	// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();

	}
}
