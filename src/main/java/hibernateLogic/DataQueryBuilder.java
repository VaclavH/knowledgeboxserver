/**
  * @author Vaclav Hnizda
  *	Knowledge Box
  *	Project 2013-14
  *	SE491-591 - Software Engineering Studio
  *
  *
  * This class sets up the query string syntax
  * so that in your classes you can focus on 
  * other things.
  */

package hibernateLogic;

import java.util.Date;


public class DataQueryBuilder {
	private DataQueryBuilder(){ }
	
	//	Create a query to request a list from the User table with the given info
	//	 Note: each string variable must be surrounded by single quotes --> "\'" 
	//   Example: "FROM User WHERE email = 'test@test.com' AND password = 'Password1'"
	//	 Source: http://docs.jboss.org/hibernate/orm/3.3/reference/en-US/html/queryhql.html
	
	/**
	 * This method will make hibernate query for the whole table. FYI!
	 * @param tableName - this is the name of the database table you want to access
	 * @return
	 */
	public static String searchString(String tableName){
		String query = "FROM " + tableName;
		return query;
	}
	
	/**
	 * This method returns a string to do a query with a one column keyword search
	 * @param tableName
	 * @param columnName
	 * @param word
	 * @return
	 */
	public static String searchString(String tableName, String columnName, String word){
		String query = "FROM " + tableName + " WHERE " + columnName + " = \'" + word + "\'";
		return query;
	}

	/**
	 * This method returns a string to do a query with a one column keyword search
	 * @param tableName
	 * @param columnName
	 * @param word
	 * @return
	 */
	public static String searchString(String tableName, String columnName, int id){
		String query = "FROM " + tableName + " WHERE " + columnName + " = \'" + id + "\'";
		return query;
	}
	
	/**
	 * This method returns a string to do a query with a two column keyword search
	 * @param tableName
	 * @param columnName
	 * @param word
	 * @param columnName2
	 * @param word2
	 * @return
	 */
	public static String searchString(String tableName, String columnName,  String word
													 , String columnName2, String word2){
		String query = "FROM " + tableName + " WHERE " + columnName  + " = \'" + word  + "\'"
										   + " AND "   + columnName2 + " = \'" + word2 + "\'";
		return query;
	}
	
	/**
	 * This method returns a string to do a query with a two column keyword search
	 * @param tableName
	 * @param columnName
	 * @param word
	 * @param columnName2
	 * @param word2
	 * @return
	 */
	public static String searchString(String tableName, String columnName,  int id
			, String columnName2, String word2){
		String query = "FROM " + tableName + " WHERE " + columnName  + " = \'" + id  + "\'"
				+ " AND "   + columnName2 + " = \'" + word2 + "\'";
		return query;
	}
	
	
	/**
	 * This method returns a string to do a query with a two column keyword search
	 * @param tableName
	 * @param columnName
	 * @param word
	 * @param columnName2
	 * @param word2
	 * @return
	 */
	public static String searchString(String tableName, String columnName,  int id
			, String columnName2, int id2){
		String query = "FROM " + tableName + " WHERE " + columnName  + " = \'" + id  + "\'"
				+ " AND "   + columnName2 + " = \'" + id2 + "\'";
		return query;
	}
	
	/**
	 * This method returns a string to do a query with a three column keyword search
	 * @param tableName
	 * @param columnName
	 * @param word
	 * @param columnName2
	 * @param word2
	 * @param columnName3
	 * @param word3
	 * @return
	 */
	public static String searchString(String tableName, String columnName,  String word
													  , String columnName2, String word2
													  , String columnName3, String word3){
		String query = "FROM " + tableName + " WHERE " + columnName  + " = \'" + word  + "\'"
										   + " AND "   + columnName2 + " = \'" + word2 + "\'"
										   + " AND "   + columnName3 + " = \'" + word3 + "\'";
		return query;
	}
	
	// This is supposed to return a query to search for groups with the creation date as one of the values
	public static String searchString(String tableName, String columnName,  String word
				 , String columnName2, Date word2){
	String query = "FROM " + tableName + " WHERE " + columnName  + " = \'" + word  + "\'"
	  + " AND "   + columnName2 + " = \'" + word2 + "\'";
	return query;
	}
}