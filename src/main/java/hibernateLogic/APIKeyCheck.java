/**
 *  @author Vaclav Hnizda
 *	Knowledge Box
 *	Project 2013-14
 *	SE491-591 - Software Engineering Studio
 */

package hibernateLogic;

import java.util.List;

import hibernate.APIKey;
import hibernate.Column;
import hibernate.Table;

public class APIKeyCheck {
	
	private APIKeyCheck(){}
	
	public static boolean exists(int apiKey){
		DatabasePro<APIKey, Integer , Integer, String> myTools = new DatabasePro<>();
		List<APIKey> myResults = myTools.getTypeList(Table.APIKey, Column.apiKey, apiKey);
		if(myResults.size() == 0)
			return false;
		else
			return true;
	}
}
