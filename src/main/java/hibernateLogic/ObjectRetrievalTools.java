/**
 *  @author Vaclav Hnizda
 *  @author Anas A. Aljuwaiber
 *	Knowledge Box
 *	Project 2013-14
 *	SE491-591 - Software Engineering Studio
 */

package hibernateLogic;

import hibernate.Table;
import hibernate.User;

import java.util.List;

import org.hibernate.Session;

/**
 * These Generic methods will allow you to pull any object from the requested table.
 * @author Vaclav Hnizda
 *
 * @param <E> This refers to the database table AKA java object you want to search
 */
public class ObjectRetrievalTools<E> {
	
	/**
	 * 
	 * @param tableName This should be the equal to the type of object you want returned
	 * @param column1Name
	 * @param word1
	 * @param column2Name
	 * @param word2
	 * @return 1 instance of the set object is returned
	 * @throws Exception is thrown if no item is found
	 */
	public E getObject(Table tableName, String column1Name, String searchWord1) throws Exception{
		
		
		// Pre-create string used for query search
		String hql = DataQueryBuilder.searchString(tableName.toString(), column1Name, 
				searchWord1);
		
		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Run a query to find the requested user
		List result = session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		if (result.size() ==0){
			throw new Exception("Could not get User object. User is not in the database!");
		}
		else{
			return (E)result.get(0);
		}
		
	}
	
	/**
	 * 
	 * @param tableName This should be the equal to the type of object you want returned
	 * @param column1Name
	 * @param word1
	 * @param column2Name
	 * @param word2
	 * @return 1 instance of the set object is returned
	 * @throws Exception is thrown if no item is found
	 */
	public E getObject(Table tableName, String column1Name, int searchID) throws Exception{
		
		
		// Pre-create string used for query search
		String hql = DataQueryBuilder.searchString(tableName.toString(), column1Name, 
				searchID);
		
		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Run a query to find the requested user
		List result = session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		if (result.size() ==0){
			throw new Exception("Could not get User object. User is not in the database!");
		}
		else{
			return (E)result.get(0);
		}
		
	}
	
	
	/**
	 * 
	 * @param tableName This should be the equal to the type of object you want returned
	 * @param column1Name
	 * @param word1
	 * @param column2Name
	 * @param word2
	 * @return 1 instance of the set object is returned
	 * @throws Exception is thrown if no item is found
	 */
	public E getObject(Table tableName, String column1Name, String searchWord1, 
							String column2Name, String searchWord2) throws Exception{
		
		
		// Pre-create string used for query search
		String hql = DataQueryBuilder.searchString(tableName.toString(), column1Name, 
													searchWord1, column2Name, searchWord2);

		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
   		// Run a query to find the requested user
		List result = session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		if (result.size() ==0){
			throw new Exception("Could not get User object. User is not in the database!");
		}
		else{
			return (E)result.get(0);
		}
	}
	
	/**
	 * 
	 * @param tableName This should be the equal to the type of object you want returned
	 * @param column1Name
	 * @param word1
	 * @param column2Name
	 * @param word2
	 * @return 1 instance of the set object is returned
	 * @throws Exception is thrown if no item is found
	 */
	public E getObject(Table tableName, String column1Name, String searchWord1, 
			String column2Name, int searchWord2) throws Exception{
		
		
		// Pre-create string used for query search
		String hql = DataQueryBuilder.searchString(tableName.toString(), column2Name, 
				searchWord2, column1Name, searchWord1);
		
		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Run a query to find the requested user
		List result = session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		if (result.size() ==0){
			throw new Exception("Could not get User object. User is not in the database!");
		}
		else{
			return (E)result.get(0);
		}
	}
	
	
	
	/**
	 * 
	 * @param tableName This should be the equal to the type of object you want returned
	 * @param column1Name
	 * @param word1
	 * @param column2Name
	 * @param word2
	 * @return 1 instance of the set object is returned
	 * @throws Exception is thrown if no item is found
	 */
	public E getObject(Table tableName, String column1Name, int searchNumber1, 
			String column2Name, int searchNumber2) throws Exception{
		
		
		// Pre-create string used for query search
		String hql = DataQueryBuilder.searchString(tableName.toString(), column1Name, 
				searchNumber1, column2Name, searchNumber2);
		
		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Run a query to find the requested user
		List result = session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		if (result.size() ==0){
			throw new Exception("Could not get User object. User is not in the database!");
		}
		else{
			return (E)result.get(0);
		}
	}
	
	
	/**
	 * 
	 * @param tableName This should be the equal to the type of object you want returned
	 * @param column1Name
	 * @param word1
	 * @param column2Name
	 * @param word2
	 * @return 1 instance of the set object is returned
	 * @throws Exception is thrown if no item is found
	 */
	public E getObject(Table tableName, String column1Name, String searchWord1, 
			String column2Name, String searchWord2,
			String column3Name, String searchWord3) throws Exception{
		
		
		// Pre-create string used for query search
		String hql = DataQueryBuilder.searchString(tableName.toString(), column1Name, 
				searchWord1, column2Name, searchWord2, column3Name, searchWord3);
		
		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Run a query to find the requested user
		List result = session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		if (result.size() ==0){
			throw new Exception("Could not get User object. User is not in the database!");
		}
		else{
			return (E)result.get(0);
		}		
	}
	
	/**
	 * This search returns a list of Users found.
	 * @return
	 * @throws Exception
	 */
	public List<E> getObjectList(Table table) throws Exception{
		
		// Pre-create string used for query search
		String hql = DataQueryBuilder.searchString(table.toString());
		
		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Run a query to find the requested user
		List<E> result = (List<E>)session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		if (result.size() ==0){
			throw new Exception("Could not get the " + table.toString() +" List. Database is empty or offline!");
		}
		else{
			return result;
		}
	}

	
	/**
	 * This search returns a list of Users found.
	 * @return
	 * @throws Exception
	 */
	public List<E> getObjectList(Table table, String column1Name, String searchWord1) throws Exception{
		
		// Pre-create string used for query search
		String hql = DataQueryBuilder.searchString(table.toString(),column1Name,searchWord1);
		
		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Run a query to find the requested user
		List<E> result = (List<E>)session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		if (result.size() ==0){
			throw new Exception("Could not get the " + table.toString() +" List. Database is empty or offline!");
		}
		else{
			return result;
		}
	}
	
	
	/**
	 * This search returns a list of Users found.
	 * @return
	 * @throws Exception
	 */
	public List<E> getObjectList(Table table, String column1Name, String searchWord1,
									String column2Name, String searchWord2) throws Exception{
		
		// Pre-create string used for query search
		String hql = DataQueryBuilder.searchString(table.toString(),column1Name,searchWord1,column2Name,searchWord2);
		
		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Run a query to find the requested user
		List<E> result = (List<E>)session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		if (result.size() ==0){
			throw new Exception("Could not get the " + table.toString() +" List. Database is empty or offline!");
		}
		else{
			return result;
		}
	}
	
	/**
	 * This search returns a list of Users found.
	 * @return
	 * @throws Exception
	 */
	public List<E> getObjectList(Table table, String column1Name, Integer searchWord1) throws Exception{
		
		// Pre-create string used for query search
		String hql = DataQueryBuilder.searchString(table.toString(),column1Name,searchWord1);
		
		// Open a new session through Hibernate
		Session session = MyHibernateFactory.newSession();
		session.beginTransaction();
		
		// Run a query to find the requested user
		List<E> result = (List<E>)session.createQuery(hql).list();
		
		// Commit & Close the session when you are done with the database!
		session.getTransaction().commit();
		session.close();
		
		if (result.size() ==0){
			throw new Exception("Could not get the " + table.toString() +" List. Database is empty or offline!");
		}
		else{
			return result;
		}
	}
}