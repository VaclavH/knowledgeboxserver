/**
 *  @author Vaclav Hnizda
 *	Knowledge Box
 *	Project 2013-14
 *	SE491-591 - Software Engineering Studio
 */

package question;

public class QuestionStatusJson {
	
	private boolean questionStatus;
	
	private QuestionStatusJson(){}
	
	public QuestionStatusJson(boolean questionStatus){
		this.questionStatus = questionStatus;
	}
	
	public boolean getQuestionStatus(){
		return questionStatus;
	}
}
