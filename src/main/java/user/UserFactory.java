/**
  * @author Paul Pelafas
  * @author Vaclav Hnizda
  *	Knowledge Box
  *	Project 2013-14
  *	SE491-591 - Software Engineering Studio
  */

package user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hibernate.Answer;
import hibernate.User;

/**
 * This class is used to prep class objects for JSON. 
 * These objects are restricted to house only the information that
 * the user needs (removing all extra information)
 */
public class UserFactory {
	/**
	 * Constructor set to private to prevent instantiation of an object of this class name
	 */
	private UserFactory() {}
	/**
	 * Method is used to create the User Status objects with a passed in parameter
	 * @param loginStatus is the passed in parameter to be set by the method
	 * @return is the returned StatusOutput object after creation
	 */
	public static UserStatusJson validUser(boolean loginStatus) {
		
		UserStatusJson output = new UserStatusJson(loginStatus);
		return output;		
	}
	
	/**
	 *  Method used to create a new user
	 */
	public static User createUser(String firstName, String lastName,
			String email, String password) {
		// 1. Create a Date Stamp
		Date createDate = new Date();
		
		// 2. Create a new User class
		User newUser = new User(email,password,firstName,lastName,createDate);
		newUser.setModifyDate(createDate);
		
		// 3. Return User
		return newUser;
	}
	
	/**
	 * Method is used to create a new UserNameJson
	 * @param allUsers
	 * @return
	 */
	public static UserNameJson createUserNameJson(String firstName, String lastName) {
		
		return new UserNameJson(firstName, lastName);

	}
	
	/**
	 * Method to create a new UserNameJson with date joined field
	 * @param firstName
	 * @param lastName
	 * @param dateJoined
	 * @return
	 */
	public static UserNameJson createUserNameJson(String firstName, String lastName, Date dateJoined) {
		
		return new UserNameJson(firstName, lastName, dateJoined);
	}
	
	/**
	 * Method is used to create a new List of UserNameJson
	 * @param allUsers
	 * @return
	 */
	public static List<UserNameJson> createUserNameList() {
		
		return new ArrayList<UserNameJson>();
		
	}
	
	/**
	 * Method to return an object with the user ID
	 * @param id
	 * @return
	 */
	public static UserIDJson id(int id){
		return new UserIDJson(id);
	}

}

