/**
 *  @author Vaclav Hnizda
 *	Knowledge Box
 *	Project 2013-14
 *	SE491-591 - Software Engineering Studio
 */

package user;

public class UserIDJson {
	
	private int userID;
	
	private UserIDJson(){}
	
	public UserIDJson(int userID){
		this.userID = userID;
	}
	
	public int getUserID(){
		return userID;
	}
}
