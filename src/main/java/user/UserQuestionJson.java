/**
 *  @author Vaclav Hnizda
 *	Knowledge Box
 *	Project 2013-14
 *	SE491-591 - Software Engineering Studio
 */

package user;

import java.util.Date;

public class UserQuestionJson {
	private String questionText, questionTypeName;
	private Date creationDate;
	/**
	 * @return the questionName
	 */
	public String getQuestionText() {
		return questionText;
	}
	/**
	 * @param questionName the questionName to set
	 */
	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}
	/**
	 * @return the questionTypeName
	 */
	public String getQuestionTypeName() {
		return questionTypeName;
	}
	/**
	 * @param questionTypeName the questionTypeName to set
	 */
	public void setQuestionTypeName(String questionTypeName) {
		this.questionTypeName = questionTypeName;
	}
	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	
}
