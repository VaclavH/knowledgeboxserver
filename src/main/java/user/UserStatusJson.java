/** 
  *  @author Charles Kwiatkowski 
  *  Knowledge Box
  *  Project 2013-14
  *  SE491-591 - Software Engineering Studio
  */

package user;

/*
 * This class is used to prepare a clean JSON class file
 */

public class UserStatusJson {
	// Instance variable status reflects the status of 
	// this user instance
	private boolean userStatus;
	
	// Default constructor set to private
	private UserStatusJson(){}
	
	// Non default constructor allowed. Initializes the status
	// to the value passed
	public UserStatusJson(boolean userStatus)
	{
		this.userStatus = userStatus;
	}
	
	public boolean getUserStatus()
	{
		return userStatus;
	}

}
