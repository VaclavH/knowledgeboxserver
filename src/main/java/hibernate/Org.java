package hibernate;

import java.util.Date;

/**
 * 
 * @author Paul Pelafas
 * Class is used by Hibernate to communicate with the database
 */
public class Org implements HibernateTable {
	
	private int id;
	
	private String orgName;
	
	private Date createDate, modifyDate;
	
	/**
	 * Empty constructor
	 */
	public Org(){}
	
	/**
	 * Constructor takes a String and calls the mutator to set the orgName field
	 * @param orgName
	 */
	public Org(String orgName, Date createDate){
		
		this.setOrgName(orgName);
		this.setCreateDate(createDate);
	
	}
	
	/**
	 * Method returns the id
	 * @return is the id
	 */
	public int getId(){
		return id;
	}
	
	/**
	 * Method sets the id
	 * @param id is the parameter to set to the local variable
	 */
	private void setId(int id){
		this.id = id;
	}
	
	/**
	 * Method returns the orgName
	 * @return is the orgName
	 */
	public String getOrgName(){
		return orgName;
	}
	
	/**
	 * Method sets the orgName
	 * @param orgName is the parameter to be set to the local variable
	 */
	public void setOrgName(String orgName){
		this.orgName = orgName;
	}
	
	
	/**
	 * Method gets the createDate
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Method sets the createDate
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * Method gets the modifyDate
	 * @return the modifyDate
	 */
	public Date getModifyDate() {
		return modifyDate;
	}

	/**
	 * Method sets the modifyDate
	 * @param modifyDate the modifyDate to set
	 */
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
}
