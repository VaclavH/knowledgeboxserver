package hibernate;

import java.util.Date;

/**
 * 
 * @author Paul Pelafas
 * Class is used by Hibernate to communicate with the database
 */
public class UserRoles implements HibernateTable{
	
	private int id;
	
	private String roleName;
	
	private Date createDate;
	
	/**
	 * Empty constructor
	 */
	public UserRoles(){}
	
	/**
	 * Constructor calls mutator that sets the local field
	 * @param roleName is the passed parameter
	 */
	public UserRoles(String roleName,Date createDate){
		setRoleName(roleName);
		setCreateDate(createDate);
	}
	
	/**
	 * Method gets the id
	 * @return is the id
	 */
	public int getId(){
		return id;
	}
	
	/**
	 * Method sets the id 
	 * @param id is the parameter to be set to the local field
	 */
	private void setId(int id){
		this.id = id;
	}
	
	/**
	 * Method gets the roleName
	 * @return is the roleName
	 */
	public String getRoleName(){
		return roleName;
	}
	
	/**
	 * Method sets the roleName
	 * @param roleName is the parameter to be set to the local field
	 */
	public void setRoleName(String roleName){
		this.roleName = roleName;
	}
	
	/**
	 * Method gets the createDate
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * Method sets the createDate
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
