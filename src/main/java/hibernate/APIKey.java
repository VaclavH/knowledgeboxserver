/**
  *  @author Vaclav Hnzida
  *	 Knowledge Box
  *	 Project 2013-14
  *	 SE491-591 - Software Engineering Studio
  */


/*
 * This is a place holder
 * we will use this to create a database table 
 * to hold all the API keys for developers.
 */
package hibernate;

public class APIKey implements HibernateTable {
	private int id,
				userId,
				apiKey;
	private String companyName;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	private void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the apiKey
	 */
	public int getApiKey() {
		return apiKey;
	}

	/**
	 * @param apiKey the apiKey to set
	 */
	public void setApiKey(int apiKey) {
		this.apiKey = apiKey;
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
}
