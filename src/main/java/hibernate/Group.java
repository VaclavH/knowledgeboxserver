/**
 * 
 */
package hibernate;

import java.util.Date;

/**
 * @author Charles Kwiatkowski
 * 			This class contains the functionality of Groups, as described
 * 			by the use case documentation
 * 			Note that use of the mutators changes the modifyDate.
 */
public class Group implements HibernateTable {

	// Instance variable declarations
	// Even though private is default, it is good to be specific
	private int id, orgId;
	private String groupTitle,	groupDesc;
	private Date 	createDate,	modifyDate;
					//lonelyWishIHadADate;
	
	//Default constructor
	public Group(){
		// nothing
	}
	


	public Group(String groupTitle, String groupDesc, int orgId)
	{
		this.setGroupTitle(groupTitle);
		this.setGroupDesc(groupDesc);
		this.setCreateDate(new Date());
		this.setOrgId(orgId);
	}
	
	/**
	 * @return the groupId
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param groupId the groupId to set
	 */
	private void setId(int groupId) {
		this.id = groupId;
	}

	/**
	 * @return the orgId
	 */
	public int getOrgId() {
		return orgId;
	}

	/**
	 * @param orgId the orgId to set
	 */
	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}

	/**
	 * @return the groupTitle
	 */
	public String getGroupTitle() {
		return groupTitle;
	}

	/**
	 * @param groupTitle the groupTitle to set
	 */
	public void setGroupTitle(String groupTitle) {
		this.groupTitle = groupTitle;
	}

	/**
	 * @return the groupDesc
	 */
	public String getGroupDesc() {
		return groupDesc;
	}

	/**
	 * @param groupDesc the groupDesc to set
	 */
	public void setGroupDesc(String groupDesc) {
		this.groupDesc = groupDesc;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the modifyDate
	 */
	public Date getModifyDate() {
		return modifyDate;
	}

	/**
	 * @param modifyDate the modifyDate to set
	 */
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
} // end of Group class
