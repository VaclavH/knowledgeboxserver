/**
 *  @author Sabrina Guillaume
 *	Knowledge Box
 *	Project 2013-14
 *	SE491-591 - Software Engineering Studio
 */

package hibernate;

import java.util.Date;

public class User implements HibernateTable {

	private int id,
				orgId;
	
	private String 	email,
				   	password,
				   	firstName,
				   	lastName;
	
	private Date createDate,
				 modifyDate;
	
	public User() {
		// this form used by Hibernate
	}
	
	public User(String email,String password, String firstName, String lastName, Date createDate) {
		setEmail(email);
		setPassword(password);
		setFirstName(firstName);
		setLastName(lastName);
		setCreateDate(createDate);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	private void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the orgId
	 */
	public int getOrgId() {
		return orgId;
	}
	
	/**
	 * @param orgId the orgId to set
	 */
	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * @return the fName
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * @param fName the fName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * @return the lName
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * @param lName the email to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}
	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * @return the modifyDate
	 */
	public Date getModifyDate() {
		return modifyDate;
	}
	/**
	 * @param modifyDate the modifyDate to set
	 */
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
}