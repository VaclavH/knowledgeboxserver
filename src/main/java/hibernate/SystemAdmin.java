/**
  *  @author Vaclav Hnzida
  *	 Knowledge Box
  *	 Project 2013-14
  *	 SE491-591 - Software Engineering Studio
  */


/*
 * This is a place holder
 * we will use this to create a database table 
 * to hold all the system admin info.
 */
package hibernate;

public class SystemAdmin {
	/*
	 * TODO create a class that holds the admin key 
	 * and accounts that might be linked to it.
	 * 
	 * Table should also hold a counter for how many times
	 * this user's account was used?
	 */

}