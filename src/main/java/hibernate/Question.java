/**
 *  @author Vaclav Hnizda
 *	Knowledge Box
 *	Project 2013-14
 *	SE491-591 - Software Engineering Studio
 */

package hibernate;

import java.util.Date;

public class Question implements HibernateTable {
	
	private int id, 
				groupId,
				questionTypeId,
				userId;
	
	private String 	questionText;
	
	private Date 	createDate,
					modifyDate;
	
	public Question() {
		// this form used by Hibernate
	}
	
	public Question(String question) {
		setQuestionText(question);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	private void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the topicId
	 */
	public int getGroupId() {
		return groupId;
	}
	/**
	 * @param topicId the topicId to set
	 */
	public void setGroupId(int topicId) {
		this.groupId = topicId;
	}
	/**
	 * @return the questionTypeId
	 */
	public int getQuestionTypeId() {
		return questionTypeId;
	}
	/**
	 * @param questionTypeId the questionTypeId to set
	 */
	public void setQuestionTypeId(int questionTypeId) {
		this.questionTypeId = questionTypeId;
	}
	/**
	 * @return the questionText
	 */
	public String getQuestionText() {
		return questionText;
	}
	/**
	 * @param questionText the questionText to set
	 */
	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}
	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}
	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * @return the modifyDate
	 */
	public Date getModifyDate() {
		return modifyDate;
	}
	/**
	 * @param modifyDate the modifyDate to set
	 */
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
}
