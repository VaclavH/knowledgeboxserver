/**
 * @author Paul Pelafas
 * Knowledge Box
 * 2013-01-22
 */

package group;

/**
 * This class is used to get/set status of a group 
 */
public class GroupStatusJson {
	
	private boolean groupStatus;
	
	/**
	 * Default constructor set to private
	 */
	private GroupStatusJson(){}
	
	/**
	 * Constructor takes a boolean value and sets status 
	 * @param groupStatus is the incoming boolean parameter
	 */
	public GroupStatusJson(boolean groupStatus){
		this.groupStatus = groupStatus;
	}
	
	/**
	 * This method returns the group status as a boolean answer
	 * @return is the return value
	 */
	public boolean getGroupStatus(){
		return groupStatus;
	}

}
