package group;

import java.util.Date;

/**
 * This class is a container for holding needed information to be passed out from the API
 * @author Vaclav Hnizda
 *
 */
public class GroupDetailsJson {
	
	private int iD;
	private String groupTitle;
	private int numOfMembers;
	private int numOfQuestions;
	private Date createDate;
	/**
	 * @return the iD
	 */
	public int getID() {
		return iD;
	}
	/**
	 * @param iD the iD to set
	 */
	public void setID(int iD) {
		this.iD = iD;
	}
	/**
	 * @return the grouName
	 */
	public String getGroupTitle() {
		return groupTitle;
	}
	/**
	 * @param grouName the grouName to set
	 */
	public void setGroupTitle(String groupTitle) {
		this.groupTitle = groupTitle;
	}
	/**
	 * @return the numOfMembers
	 */
	public int getNumOfMembers() {
		return numOfMembers;
	}
	/**
	 * @param numOfMembers the numOfMembers to set
	 */
	public void setNumOfMembers(int numOfMembers) {
		this.numOfMembers = numOfMembers;
	}
	/**
	 * @return the numOfQuestions
	 */
	public int getNumOfQuestions() {
		return numOfQuestions;
	}
	/**
	 * @param numOfQuestions the numOfQuestions to set
	 */
	public void setNumOfQuestions(int numOfQuestions) {
		this.numOfQuestions = numOfQuestions;
	}
	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}
	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
