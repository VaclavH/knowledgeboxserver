package group;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import hibernate.Group;
import junit.framework.TestCase;

public class GroupFactoryTest extends TestCase{

	
	
	@Override
	protected void setUp() throws Exception {
	
		
	}
	
	@Test
	public void test01() {
		
		Group group = new Group();
		group.setGroupTitle("hello");
		group.setGroupDesc("World");
		GroupInfoJson infoJson = GroupFactory.createGroupInfoJson(group);
		
		if (infoJson.getTitle().equals(group.getGroupTitle())){
			
		}
		
		else{
			Assert.fail("Failed because titles do not match");
		}
		
		if (infoJson.getDescription().equals(group.getGroupDesc())){
			
		}
		
		else{
			Assert.fail("Failed because desc do not match");
		}
	}
	
	@Test
	public void test02(){
		
		Group groupTest = new Group();
		groupTest.setGroupTitle("hello");
		groupTest.setGroupDesc("world");
		int numberOfQuestions = 4;
		int numberOfMembers = 2;
		
		GroupDetailsJson groupDetails = GroupFactory.createGroupDetailsJson(groupTest, numberOfQuestions, numberOfMembers);
		//groupDetails.setCreateDate(groupTest.getCreateDate());
		
		if (groupDetails.getGroupTitle().equals(groupTest.getGroupTitle())){
			
		}
		else {
			Assert.fail("Failed because titles do not match");
		}
		
		if (groupDetails.getNumOfMembers() == 2) {
			
		}
		else {
			Assert.fail("Failed because number of members did not match");
		}
		
		if (groupDetails.getNumOfQuestions() == 4) {
			
		}
		else {
			Assert.fail("Failed because the number of questions did not match");
		}
		
	}
	
}
