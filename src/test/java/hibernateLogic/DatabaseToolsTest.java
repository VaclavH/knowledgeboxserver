package hibernateLogic;

import java.util.Date;

import group.GroupFactory;
import hibernate.Group;
import hibernate.User;
import hibernate.UserRoleGroup;

import org.hibernate.cfg.Configuration;
import org.junit.Assert;

import user.UserFactory;
import junit.framework.TestCase;

public class DatabaseToolsTest extends TestCase{
	private DatabaseTools myTools;
	
	@Override
	protected void setUp() throws Exception {
		// A SessionFactory is set up once for an application
        myTools = new DatabaseTools();
	}
	
	public void testUserCreate01() {
		
		// create a new user
		User newUser = UserFactory.createUser("John", "Doe", "jd@gmail.com", "Password5");
		
		// Enter user into the database and get the ID back
		int myUserID = myTools.dataEntry(newUser);
		
		if(myUserID <= 0) {Assert.fail(" User ID was suppose to be > 0 but instead it was " + myUserID);}
	}
	
	public void testGroupCreate01() {
		
		// create a new group
		Group newGroup = GroupFactory.createGroup("Math", "Algebra study group");
		
		// Enter group into the database and get the ID back
		int myGroupID = myTools.dataEntry(newGroup);
		
		if(myGroupID <= 0) {Assert.fail(" Group ID was suppose to be > 0 but instead it was " + myGroupID);}
	}
	
	public void testGroupRoles01() {
		
		// create a new user
		int userID = myTools.dataEntry(UserFactory.createUser("Martha", "Stewart", "MS@hotmail.com", "Password7"));
		// create a new group
		int groupID = myTools.dataEntry(GroupFactory.createGroup("English", "my special group"));
		// create a new Role to link primary user
		UserRoleGroup newRole = new UserRoleGroup(userID, groupID, 0, new Date());
		
		// Enter info into the database and get the ID back
		int UserRoleID = myTools.dataEntry(newRole);
		
		if(UserRoleID <= 0) {Assert.fail("New Group Roles was suppose to have an ID > 0 but instead it was " + UserRoleID);}
	}
	
}
