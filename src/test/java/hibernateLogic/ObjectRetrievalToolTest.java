/**
 *  @author Vaclav Hnizda
 *	Knowledge Box
 *	Project 2013-14
 *	SE491-591 - Software Engineering Studio
 */

package hibernateLogic;

import java.util.Date;
import java.util.List;

import org.junit.Assert;

import hibernate.Table;
import hibernate.User;
import junit.framework.TestCase;

public class ObjectRetrievalToolTest extends TestCase{
	DatabaseTools myTools;
	
	@Override
	protected void setUp() throws Exception {
		// A SessionFactory is set up once for an application
        myTools = new DatabaseTools();        	
	}
	
	public void testOneColumnQuery() throws Exception{
		
		ObjectRetrievalTools<User> myObjectTool = new ObjectRetrievalTools<User>();
        
        User newUser = new User("test@gmail.com","Password1","John","Anderson",new Date());
        myTools.dataEntry(newUser);
        
        User retrievedUser = myObjectTool.getObject(Table.User, "email", newUser.getEmail());
        
        //check to see if the names equal each other, that means retrieval tool is working
        if(!(retrievedUser.getFirstName().equals(newUser.getFirstName()))){
        	Assert.fail(" The entered name is " + newUser.getFirstName() + 
        				" but the retrieved name is " + retrievedUser.getFirstName());
        }
        
        myTools.dataRemoval(retrievedUser);
	}
	
	public void testPullList() throws Exception{
ObjectRetrievalTools<User> myObjectTool = new ObjectRetrievalTools<User>();
        
        User newUser = new User("test1@gmail.com","Password1","Tom","Roberts",new Date());
        myTools.dataEntry(newUser);
        User newUser2 = new User("test2@gmail.com","Password1","John","Anderson",new Date());
        myTools.dataEntry(newUser2);
        
        List<User> retrievedUserList = myObjectTool.getObjectList(Table.User);
        
        //check to see if the names equal each other, that means retrieval tool is working
        if(!(retrievedUserList.size() >= 2)){
        	Assert.fail(" The list size should be 2 or greater but instead it is " + retrievedUserList.size());
        }
	}
}
