/**
 * @author Paul Pelafas
 * Knowledge Box
 */
package question;

import hibernate.Answer;
import hibernate.Question;
import junit.framework.*;

import org.junit.Test;
import org.junit.Assert;

public class QuestionFactoryTest extends TestCase{
	
	
	protected void setUp() throws Exception {
		
	}
	
	@Test
	public void test01(){
		Question question = QuestionFactory.createQuestionMC(1, 1, "What is the meaning of life?");

		Question questionTest = new Question();
		questionTest.setGroupId(1);
		questionTest.setQuestionText("What is the meaning of life?");
		
		if (questionTest.getGroupId() == question.getGroupId()){
			
		}
		else{
			Assert.fail("Group id's do not match");
		}
		
		if (questionTest.getQuestionText().equals(question.getQuestionText())){
			
		}
		else{
			Assert.fail("Question text does not match");
		}
	}
	
	@Test
	public void test02(){
		Answer answer = QuestionFactory.createAnswer("Love", 1, 1, false);
		
		Answer answerTester = new Answer("Love");
		answerTester.setSortOrder(1);
		answerTester.setQuestionId(1);
		answerTester.setIsCorrect(false);
		
		if (answerTester.getAnswerText().equals(answer.getAnswerText())){
			
		}
		else {
			Assert.fail("Answer text does not match");
		}
		
		if (answerTester.getSortOrder() == answer.getSortOrder()){
			
		}
		else {
			Assert.fail("Sort order does not match");
		}
		
		if (answerTester.getQuestionId() == answer.getQuestionId()) {
			
		}
		else {
			Assert.fail("Question id's do not match");
		}
		
		if (answerTester.isIsCorrect() == answer.isIsCorrect()){
			
		}
		else {
			Assert.fail("Is correct boolean values do not match");
		}
	}

}